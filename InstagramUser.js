var request     = require("request");

class InstagramUser {
    constructor(userid) {
        if(userid) this.userid = userid;
    }
    static async makeRequest(options, sleep = 0) {
        return new Promise((resolve, reject) => {
            request.get(options, (err, res, body) => {
                if(err) return reject(err);
                var json = JSON.parse(body);
                if(json.status != 'ok') return reject(new Error(json.message));
                setTimeout(() => {
                    resolve(json);
                }, sleep);
            });
        });
    }
    static async queryGraphql(variables, query_hash, sleep = 0) {
        return (await InstagramUser.makeRequest({
            url: 'https://www.instagram.com/graphql/query/',
            qs: {
                query_hash,
                variables: JSON.stringify(variables)
            },
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
            }
        })).data.user;
    }
    async getInfo() {
        var variables = {
            user_id: this.userid,
            include_chaining: false,
            include_reel: true,
            include_suggested_users: false,
            include_logged_out_extras: false,
            include_highlight_reels: false
        }
        return (await InstagramUser.queryGraphql(variables, 'aec5501414615eca36a9acf075655b1e')).reel.owner;
    }
    async getPosts(after, count = 12) {
        var variables = {
            id: this.userid,
            first: count,
            after
        }
        return (await InstagramUser.queryGraphql(variables, '58b6785bea111c67129decbe6a448951', 100)).edge_owner_to_timeline_media;
    }
    async getAllPosts() {
        var posts = [];
        var hasNext = true;
        var after = undefined;
        while(hasNext) {
            var data = await this.getPosts(after);
            posts = posts.concat(data.edges.map(edge => edge.node));
            hasNext = data.page_info.has_next_page;
            after = data.page_info.end_cursor;
        }
        return posts;
    }
}
module.exports = InstagramUser;