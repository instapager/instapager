const InstagramUser = require('./InstagramUser');
var express         = require('express');

var data = undefined;

var app             = express();
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", req.get('origin'));
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});
app.get('/', function (req, res) {
    if(data === undefined) return res.send(500);
    res.json(data);
});
var port = process.env.PORT || 8081;
app.listen(port, '0.0.0.0');
console.log('* backend started on port ' + port);

var instagram = new InstagramUser(process.env.USERID || require('./config.json').userid);
async function scheduler() {
    try {
        var profile = await instagram.getInfo();
        var posts = await instagram.getAllPosts();
        data = {
            userid: profile.id,
            profile_pic: profile.profile_pic_url,
            username: profile.username,
            posts: posts.map(post => {
                var location;
                if(post.location) location = {
                    name: post.location.name,
                    link: `https://www.instagram.com/explore/locations/${post.location.id}/${post.location.slug}/`
                };
                return {
                    image: post.display_url,
                    thumb: post.thumbnail_src,
                    link: 'https://www.instagram.com/p/' + post.shortcode,
                    desc: post.edge_media_to_caption.edges.map(edge => edge.node.text).toString(),
                    timestamp: new Date(post.taken_at_timestamp * 1e3),
                    likes: post.edge_media_preview_like.count,
                    location
                };
            })
        };
        console.log(`* found ${posts.length} posts for ${profile.username}`);
        setTimeout(scheduler, 60*1e3);
    } catch(err) {
        console.error('* failed to get data: ', err.toString());
        setTimeout(scheduler, 5*60*1e3);
    }
}
scheduler();